import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView'

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Admin from './pages/Admin';
import Create from './pages/Create';
import Retrieve from './pages/Retrieve';
import Update from './pages/Update';
/*import Error from './pages/Error';*/

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
 
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
     method: 'POST',
      headers: {

        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
            // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
    
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
  })
}, [])

  return (
   
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          < AppNavbar/>
          <Container>
          <Routes>
            < Route path="/" element={<Home/>}/>
           < Route path="/products" element={<Products/>}/>
            < Route path="/products/:productsId" element={<ProductView/>}/>
            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login />}/>
            < Route path="/logout" element={<Logout/>}/>
            < Route path="/admin" element={<Admin/>}/>
            < Route path="/create" element={<Create/>}/>
            < Route path="/retrieve" element={<Retrieve/>}/>
            < Route path="/update" element={<Update/>}/>
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
