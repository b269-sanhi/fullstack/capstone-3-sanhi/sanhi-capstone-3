import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext'
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom'
export default function ProductView() {
	
	const navigate = useNavigate()
	const {user} = useContext(UserContext)
	const {productsId} = useParams()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [stock, setStock] = useState("");
	const [price, setPrice] = useState(0);

	function orderCreate() {
		fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				'productId': productsId,
				'quantity': 1
			})
		})
		.then(res => res.json()
		)
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully ordered",
					icon: "success",
					text: "You have successfully ordered."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productsId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setStock(data.stock);
			})
	}, [productsId])

	return (

	<Container>
		<Row>
			<Col lg={{span: 6, offset:3}} >
				<Card>
					 <Card.Body className="text-center">
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
		 		        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>
				        <Card.Subtitle>Stock:</Card.Subtitle>
				        <Card.Text>{stock}</Card.Text>
				   
					        
					   	 {
  user.isAdmin ?
    <Button variant="primary" onClick={() => orderCreate()} disabled>Order</Button> // disable button if user is admin
    :
    <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
}


					  </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}