import { useState } from 'react';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function Create() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  

  const addProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
      body: JSON.stringify
      ({ 
      name : name,
      description : description,
      price : price,
      stock : stock
       })
    })
      .then(res => res.json())
      .then(data => {
        setName('');
        setDescription('');
        setPrice('');
        setStock('');
        Swal.fire({
          title: "Product Created Successfully!",
          icon: "success",
          text: "Thank you!"
        }); 
      })
  }

  return (
    <Form onSubmit={e => addProduct(e)}>
      <Form.Group className="mb-1" controlId="name">
        <Form.Label>Name</Form.Label>
        <Form.Control as="textarea" value={name} placeholder="name of product" onChange={(e) => setName(e.target.value)} />
      </Form.Group>
      <Form.Group className="mb-1" controlId="description">
        <Form.Label>Description:</Form.Label>
        <Form.Control as="textarea" value={description} rows={3} onChange={(e) => setDescription(e.target.value)} />
      </Form.Group>
      <Form.Group className="mb-1" controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
      </Form.Group>
      <Form.Group className="mb-1" controlId="stock">
        <Form.Label>Stock</Form.Label>
        <Form.Control type="number" value={stock} onChange={(e) => setStock(e.target.value)} />
      </Form.Group>
      <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
        Submit
      </Button>
    </Form>
  );
}
