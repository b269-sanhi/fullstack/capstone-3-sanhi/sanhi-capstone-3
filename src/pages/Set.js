
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function Product({ product }) {
  const [active, setActive] = useState(product.active);

  const handleStatusUpdate = () => {
    const newStatus = !active;

    fetch(`${process.env.REACT_APP_API_URL}/products/:courseId/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ active: newStatus })
    })
      .then(res => res.json())
      .then(data => {
        setActive(newStatus);
        Swal.fire({
          title: "Product status updated successfully!",
          icon: "success",
          text: "Thank you!"
        });
      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <div>
      <h2>{product.name}</h2>
      <p>{product.description}</p>
      <p>{product.price}</p>
      <p>{product.stock}</p>
      <button onClick={handleStatusUpdate}>
        {active ? 'Deactivate' : 'Activate'}
      </button>
    </div>
  );
}
