import { useState, useEffect } from 'react';
import {useParams} from 'react-router-dom'
export default function UpdateProduct({ match }) {
  const {productId} = useParams('')
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [inStock, setInStock] = useState(false);
  const [isUpdating, setIsUpdating] = useState(false);

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/_id}`);
        const data = await response.json();
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setInStock(data.inStock);
      } catch (error) {
        console.error(error);
      }
    };
    fetchProduct();
  }, [productId]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsUpdating(true);
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/_id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name, description, price, inStock }),
      });
      const data = await response.json();
      setIsUpdating(false);
      alert('Product updated successfully');
    } catch (error) {
      console.error(error);
      setIsUpdating(false);
      alert('Error occurred while updating the product');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div>
        <label htmlFor="description">Description:</label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </div>
      <div>
        <label htmlFor="price">Price:</label>
        <input
          type="number"
          id="price"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </div>
      <div>
        <label htmlFor="inStock">In Stock:</label>
        <input
          type="checkbox"
          id="inStock"
          checked={inStock}
          onChange={(e) => setInStock(e.target.checked)}
        />
      </div>
      <button type="submit" disabled={isUpdating}>Update Product</button>
    </form>
  );
}

